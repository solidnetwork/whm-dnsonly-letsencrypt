#!/usr/local/cpanel/3rdparty/bin/python

import os, sys, subprocess, json
from collections import namedtuple

def _json_object_hook(d):
    return namedtuple('X', d.keys(),rename=True)(*d.values())

def json2obj(data): 
    return json.loads(data, object_hook=_json_object_hook)

def doFilesExists(files=[]):
    for file in files:
        if not os.path.isfile(file):
            print "File %s does not exists or not a regular file. Exiting..." % (file)
            sys.exit(1)

def whmCmd(args=[]):
    cmd = ['/usr/local/cpanel/bin/whmapi1', '--output=json']
    cmd += args
    obj = json2obj(subprocess.check_output(cmd))
    if hasattr(obj, 'metadata') and obj.metadata.result == False:
        print "Command %s failed with error: %s" % (cmd, obj.metadata.reason)
        sys.exit(1)
    elif hasattr(obj, 'status'):
        print "Command %s failed with error: %s" % (cmd, obj.statusmsg)
        sys.exit(1)

    return obj

def getHostname():
    return whmCmd(['gethostname']).data.hostname

def installSSL(services=[], sslFiles=[]):
    for service in services:
        whmCmd([  'install_service_ssl_certificate', 
                        "service={}".format(service),
                        "crt=@{}".format(sslFiles[0]),
                        "key=@{}".format(sslFiles[1]),
                        "cabundle=@{}".format(sslFiles[2])
               ])

def restartServices(services=[]):
    for service in services:
        whmCmd(['restartservice', "service={}".format(service)])


hostname = getHostname()
caPath = "/etc/letsencrypt/live/{}".format(hostname)
print "Hostname is: %s with caPath: %s" % (hostname, caPath)

sslFiles = ["{}/cert.pem".format(caPath), "{}/privkey.pem".format(caPath), "{}/chain.pem".format(caPath)]
doFilesExists(sslFiles)
installSSL(['exim', 'dovecot', 'cpanel'], sslFiles)
restartServices(['exim', 'lmtp'])

print "Finished installing certs for hostname: %s" % (hostname)
