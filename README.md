# LetsEncrypt for WHM DNSOnly
cPanel's [WHM DNSOnly](https://documentation.cpanel.net/display/76Docs/Installation+Guide+-+cPanel+DNSONLY+Installation)
product lacks auto SSL generation for core services such as exim, 
dovecot and cpanel. While cPanel offers free SSL certs from them
or from LetsEncrypt for licensed cPanel/WHM servers this feature
is not offered on WHM DNSOnly as its a free product.

The goal of this script is to Install LetsEncrypt and auto deploy
SSL certs to core services for WHM DNSOnly.
